import Phaser from 'phaser';
import EasyStar from 'easystarjs';

import gridTilesPng from './assets/gridtiles.png';
import mapJson from './assets/map.json';
import phaserguyPng from './assets/phaserguy.png';

export default class Scene extends Phaser.Scene {
  preload() {
    this.load.image('tileset', gridTilesPng);
    this.load.tilemapTiledJSON('map', mapJson);
    this.load.image('phaserguy', phaserguyPng);
  }

  create() {
    this.input.on('pointerup', (event) => this.handleClick(event));

    this.cameras.main.setBounds(0, 0, 20 * 32, 20 * 32);

    const phaserGuy = this.add.image(32, 32, 'phaserguy');
    phaserGuy.setDepth(1);
    phaserGuy.setOrigin(0, 0.5);
    this.cameras.main.startFollow(phaserGuy);
    this.player = phaserGuy;

    // Display map
    this.map = this.make.tilemap({ key: 'map' });
    // The first parameter is the name of the tileset in Tiled and the second parameter is the key
    // of the tileset image used when loading the file in preload.
    const tiles = this.map.addTilesetImage('tiles', 'tileset');
    this.map.createStaticLayer(0, tiles, 0, 0);

    // Marker that will follow the mouse
    this.marker = this.add.graphics();
    this.marker.lineStyle(3, 0xffffff, 1);
    this.marker.strokeRect(0, 0, this.map.tileWidth, this.map.tileHeight);

    // ### Pathfinding stuff ###
    // Initializing the pathfinder
    // eslint-disable-next-line new-cap
    this.finder = new EasyStar.js();

    // We create the 2D array representing all the tiles of our map
    const grid = [];
    for (let y = 0; y < this.map.height; y++) {
      const col = [];
      for (let x = 0; x < this.map.width; x++) {
        // In each cell we store the ID of the tile, which corresponds
        // to its index in the tileset of the map ("ID" field in Tiled)
        col.push(this.getTileID(x, y));
      }
      grid.push(col);
    }
    this.finder.setGrid(grid);

    const tileset = this.map.tilesets[0];
    const properties = tileset.tileProperties;
    const acceptableTiles = [];

    // We need to list all the tile IDs that can be walked on. Let's iterate over all of them
    // and see what properties have been entered in Tiled.
    for (let i = tileset.firstgid - 1; i < tiles.total; i++) {
      // firstgid and total are fields from Tiled that indicate the range of IDs that the tiles
      // can take in that tileset
      if (!properties.hasOwnProperty(i)) {
        // If there is no property indicated at all, it means it's a walkable tile
        acceptableTiles.push(i + 1);
        continue;
      }
      if (!properties[i].collide) acceptableTiles.push(i + 1);
      if (properties[i].cost) this.finder.setTileCost(i + 1, properties[i].cost);
      // If there is a cost attached to the tile, let's register it
    }
    this.finder.setAcceptableTiles(acceptableTiles);
  }

  update() {
    const worldPoint = this.input.activePointer.positionToCamera(this.cameras.main);

    // Rounds down to nearest tile
    const pointerTileX = this.map.worldToTileX(worldPoint.x);
    const pointerTileY = this.map.worldToTileY(worldPoint.y);
    this.marker.x = this.map.tileToWorldX(pointerTileX);
    this.marker.y = this.map.tileToWorldY(pointerTileY);
    this.marker.setVisible(!this.checkCollision(pointerTileX, pointerTileY));
  }

  handleClick(pointer) {
    const x = this.cameras.main.scrollX + pointer.x;
    const y = this.cameras.main.scrollY + pointer.y;
    const toX = Math.floor(x / 32);
    const toY = Math.floor(y / 32);
    const fromX = Math.floor(this.player.x / 32);
    const fromY = Math.floor(this.player.y / 32);
    console.log(`going from (${fromX},${fromY}) to (${toX},${toY})`);

    this.finder.findPath(fromX, fromY, toX, toY, (path) => {
      if (path === null) {
        console.warn('Path was not found.');
      } else {
        console.log(path);
        this.moveCharacter(path);
      }
    });
    this.finder.calculate(); // don't forget, otherwise nothing happens
  }

  getTileID(x, y) {
    const tile = this.map.getTileAt(x, y);
    return tile.index;
  }

  checkCollision(x, y) {
    const tile = this.map.getTileAt(x, y);
    return tile.properties.collide === true;
  }

  moveCharacter(path) {
    // Sets up a list of tweens, one for each tile to walk, that will be chained by the timeline
    const tweens = [];
    for (let i = 0; i < path.length - 1; i++) {
      const ex = path[i + 1].x;
      const ey = path[i + 1].y;
      tweens.push({
        targets: this.player,
        x: { value: ex * this.map.tileWidth, duration: 200 },
        y: { value: ey * this.map.tileHeight, duration: 200 },
      });
    }

    this.tweens.timeline({
      tweens,
    });
  }
}
