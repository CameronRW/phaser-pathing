import Phaser from 'phaser';
import Scene from './scene';

const config = {
  type: Phaser.AUTO,
  width: 20 * 32,
  height: 20 * 32,
  parent: 'game',
  scene: Scene,
};

// eslint-disable-next-line no-unused-vars
const game = new Phaser.Game(config);
